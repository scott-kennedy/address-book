import { connect } from "react-redux";
import { RSAA } from "redux-api-middleware";

import { selectContact, REQUEST_CONTACTS, RECEIVE_CONTACTS } from "../actions";
import ContactList from "../components/ContactList";

const mapStateToProps = ({
  addressBookApp: { contacts, selectedContact }
}: any) => {
  return {
    contacts: Object.keys(contacts).map(key => contacts[key]),
    selectedId: selectedContact
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    fetchContacts: () => {
      dispatch({
        [RSAA]: {
          endpoint: "http://localhost:8886/contacts",
          method: "GET",
          types: [REQUEST_CONTACTS, RECEIVE_CONTACTS, RECEIVE_CONTACTS]
        }
      });
    },
    onContactClick: (id: string) => {
      dispatch(selectContact(id));
    }
  };
};

const ListContainer = connect(mapStateToProps, mapDispatchToProps)(ContactList);
export default ListContainer;
