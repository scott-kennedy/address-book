import { connect } from "react-redux";

import ContactDetails from "../components/ContactDetails";

const mapStateToProps = ({
  addressBookApp: { selectedContact, contacts }
}: any) => {
  const contactInfo = contacts[selectedContact];

  return {
    contact: contactInfo
  };
};

const DetailsContainer = connect(mapStateToProps)(ContactDetails);
export default DetailsContainer;
