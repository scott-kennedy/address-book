import * as React from "react";
import "./App.css";

// import ContactList from 'src/ContactList';
import ContactListContainer from "./containers/ContactListContainer";
import ContactDetailsContainer from "./containers/ContactDetailsContainer";

class App extends React.Component {
  public render() {
    return (
      <div className="App App-layout">
        <header className="App-header Header">
          <h1 className="App-title">Address Book</h1>
        </header>

        <aside className="Sidebar">
          <ContactListContainer />
        </aside>

        <div className="Content">
          <ContactDetailsContainer />
        </div>

        <footer className="Footer">
          &copy; Copyright Scott Kennedy 2018
          <address>
            Contact at{" "}
            <a href="mailto:skennedy84@gmail.com">skennedy84@gmail.com</a>
          </address>
        </footer>
      </div>
    );
  }
}

export default App;
