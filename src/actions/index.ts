export const SELECT_CONTACT = "SELECT CONTACT";
export function selectContact(id: string) {
  return {
    type: SELECT_CONTACT,
    payload: id
  };
}

export const REQUEST_CONTACTS = "REQUEST CONTACTS";
export function requestContacts() {
  return {
    type: REQUEST_CONTACTS
  };
}

export const RECEIVE_CONTACTS = "RECEIVE CONTACTS";
export function receiveContacts(
  contacts: any,
  error: string | undefined = undefined
) {
  return {
    type: RECEIVE_CONTACTS,
    payload: error ? new Error(error) : contacts,
    error
  };
}
