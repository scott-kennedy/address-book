import * as React from "react";
import "./ContactDetails.css";

// import emailIcon from "../assets/images/envelope-solid.svg";

interface IContactDetailsProps {
  contact: any;
}

export const ContactDetails: React.SFC<IContactDetailsProps> = props => {
  if (!props.contact) {
    return null;
  }
  const { name, email, phone, address } = props.contact;

  return (
    <article className="card">
      <h1>{name}</h1>

      <address>
        <div className="block">
          <label>Phone</label>
          <hr className="minimal" />
          <div>
            <a href={"tel:" + phone}>
              <span>{phone}</span>
            </a>
          </div>
        </div>

        <div className="block">
          <label>Email</label>
          <hr className="minimal" />
          <div>
            <a href={"mailto:" + email}>
              <span>{email}</span>
            </a>
          </div>
        </div>

        <div className="block">
          <label>Address</label>
          <hr className="minimal" />
          <div>
            <span>{address}</span>
          </div>
        </div>
      </address>
    </article>
  );
};

export default ContactDetails;
