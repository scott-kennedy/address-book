import * as React from "react";

interface IContactProps {
  id: string;
  name: string;
  selected: boolean;
  onContactClick: (id: string) => void;
}

export const ContactListItem: React.SFC<IContactProps> = props => {
  const { id, name, selected, onContactClick } = props;

  const handleClick = () => {
    onContactClick(id);
  };

  let className = "ContactList-Item";

  if (selected) {
    className += " selected";
  }

  return (
    <li className={className} onClick={handleClick}>
      {name}
    </li>
  );
};

export default ContactListItem;
