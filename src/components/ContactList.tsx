import * as React from "react";
import ContactListItem from "./ContactListItem";

import "./ContactList.css";

interface IContact {
  id: string;
  name: string;
  email: string;
}

interface IContactListProps {
  contacts: IContact[];
  selectedId: string;
  fetchContacts: () => void;
  onContactClick: (id: string) => void;
}

export default class ContactList extends React.Component<IContactListProps> {
  componentWillMount() {
    this.props.fetchContacts();
  }

  render() {
    const { contacts, selectedId, onContactClick } = this.props;

    return contacts.length ? (
      <nav>
        <h3>Contacts</h3>
        <hr />
        <ul className="ContactList">
          {contacts.map((contact: any) => (
            <ContactListItem
              key={contact.id}
              id={contact.id}
              name={contact.name}
              onContactClick={onContactClick}
              selected={contact.id === selectedId}
            />
          ))}
        </ul>
      </nav>
    ) : (
      <nav>
        <h3>Contacts</h3>
        <hr />
        <ul>
          <li>Loading contacts...</li>
        </ul>
      </nav>
    );
  }
}
