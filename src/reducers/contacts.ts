import { SELECT_CONTACT, REQUEST_CONTACTS, RECEIVE_CONTACTS } from "../actions";

interface IFluxStandardAction {
  type: string;
  payload?: any;
  error?: boolean;
  meta?: any;
}

function selectedContact(state = null, action: IFluxStandardAction) {
  switch (action.type) {
    case SELECT_CONTACT:
      return action.payload;
    default:
      return state;
  }
}

function contacts(state = {}, action: IFluxStandardAction) {
  switch (action.type) {
    case RECEIVE_CONTACTS:
      return action.error ? state : action.payload;
    default:
      return state;
  }
}

function contactsById(state = [], action: IFluxStandardAction) {
  switch (action.type) {
    case RECEIVE_CONTACTS:
      return action.error ? state : Object.keys(action.payload);
    default:
      return state;
  }
}

function isFetching(state = false, action: IFluxStandardAction) {
  switch (action.type) {
    case REQUEST_CONTACTS:
      return true;
    case RECEIVE_CONTACTS:
      return false;
    default:
      return state;
  }
}

function error(state = null, action: IFluxStandardAction) {
  switch (action.type) {
    case RECEIVE_CONTACTS:
      return action.error ? action.payload : state;
    default:
      return state;
  }
}

export default function addressBookApp(
  state: any = {},
  action: IFluxStandardAction
) {
  return {
    selectedContact: selectedContact(state.selectedContact, action),
    contacts: contacts(state.contacts, action),
    contactsById: contactsById(state.contactsById, action),
    isFetching: isFetching(state.isFetching, action),
    error: error(state.error, action)
  };
}
