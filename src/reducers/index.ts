import { combineReducers } from "redux";
import addressBookApp from "./contacts";

const rootReducer = combineReducers({
  addressBookApp
});

export default rootReducer;
