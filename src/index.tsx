import * as React from "react";
import * as ReactDOM from "react-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import { apiMiddleware } from "redux-api-middleware";

import App from "./App";
import rootReducer from "./reducers";
import "./index.css";
import "./normalize.css";
import registerServiceWorker from "./registerServiceWorker";

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(apiMiddleware),
    window["__REDUX_DEVTOOLS_EXTENSION__"] &&
      window["__REDUX_DEVTOOLS_EXTENSION__"]()
  )
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
